package flowerpower

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/thingful/httpmock"
	"github.com/thingful/thingfulx"
	"golang.org/x/net/context"
)

func makeTestFetchAndStubURLS(timeProvider thingfulx.TimeProvider) (fetchURL, stubURL string) {
	data_from := timeProvider.Now().Add(time.Hour * 24 * -NumberOfDaysToQuery).Format("2006-01-02T15:04:05Z")
	data_to := timeProvider.Now().Format("2006-01-02T15:04:05Z")

	f := "https://api-flower-power-pot.parrot.com/sensor_data/v6/sample/location/ADbiLOhuIA1426685634970?lat=56.4549073&lng=-2.9933931"

	s, _ := url.Parse("https://api-flower-power-pot.parrot.com/sensor_data/v6/sample/location/ADbiLOhuIA1426685634970")
	v := url.Values{}
	v.Set("from_datetime_utc", data_from)
	v.Add("to_datetime_utc", data_to)
	s.RawQuery = v.Encode()

	return f, s.String()
}

func TestNewFetcher(t *testing.T) {
	_, err := NewFetcher()
	assert.Nil(t, err)
}

func TestProvider(t *testing.T) {

	fetcher, _ := NewFetcher()

	providerURL, _ := url.Parse("https://www.parrot.com/")

	expected := &thingfulx.Provider{
		Name:        "Parrot - Flower Power",
		UID:         "flowerpower",
		Webpage:     providerURL,
		Description: "Parrot SA is a French wireless products manufacturer company specialized in technologies involving voice recognition, signal processing for embedded products and drones.",
	}

	got := fetcher.Provider()

	assert.Equal(t, expected, got)
}

func TestAuthenticate(t *testing.T) {

	var ff *fetcher

	f, err := NewFetcher()
	assert.Nil(t, err)

	ff = f.(*fetcher)
	ff.clientID = "client-id"
	ff.clientSecret = "client-secret"
	ff.user = "username"
	ff.password = "password"

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	inputFile, err := ioutil.ReadFile("data/valid_access.json")
	assert.Nil(t, err)

	httpmock.Reset()
	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"https://api-flower-power-pot.parrot.com/user/v1/authenticate?client_id=client-id&client_secret=client-secret&grant_type=password&password=password&username=username",
			httpmock.NewBytesResponder(http.StatusOK, inputFile),
		),
	)

	err = authenticate(client, ff)
	assert.Nil(t, err)
	assert.Equal(t, ff.accessToken, "KV6zjmPLg31a0WWxDb66c846Q67s1UsUA1Oahlw2BrryOotW")
	assert.Equal(t, ff.refreshToken, "dwTJTpvMXMHcMM3uB1rktSqAWqXQyRIFAoKMzn5ZzOjAtJsC")
}

func TestAuthenticateError(t *testing.T) {

	var ff *fetcher

	f, err := NewFetcher()
	assert.Nil(t, err)

	ff = f.(*fetcher)
	ff.clientID = "client-id"
	ff.clientSecret = "client-secret"
	ff.user = "username"
	ff.password = "password"

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	inputFile, err := ioutil.ReadFile("data/invalid_access.json")
	assert.Nil(t, err)

	httpmock.Reset()
	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"https://api-flower-power-pot.parrot.com/user/v1/authenticate?client_id=client-id&client_secret=client-secret&grant_type=password&password=password&username=username",
			httpmock.NewBytesResponder(http.StatusOK, inputFile),
		),
	)

	err = authenticate(client, ff)
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "missing 'grant_type' parameter")
}

func TestGetLocationsID(t *testing.T) {
	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	inputFile, err := ioutil.ReadFile("data/valid_location.json")
	assert.Nil(t, err)

	httpmock.Reset()
	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"https://api-flower-power-pot.parrot.com/garden/v2/configuration",
			httpmock.NewBytesResponder(http.StatusOK, inputFile),
		),
	)

	locations, err := getLocationsID(client, "access-token")
	assert.Nil(t, err)

	expected := []location{
		{
			Latitude:  56.4549073,
			Longitude: -2.9933931,
			Id:        "ADbiLOhuIA1426685634970",
			Sensor: sensor{
				Type: "Flower power 6AE1",
			},
		},
		{
			Latitude:  56.4549073,
			Longitude: -2.9933931,
			Id:        "0SjBeS5ioY1426704625302",
			Sensor: sensor{
				Type: "Flower power 6D82",
			},
		},
	}

	assert.Equal(t, expected, locations)
}

func TestGetLocationsIDError(t *testing.T) {
	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	inputFile, err := ioutil.ReadFile("data/invalid_location.json")
	assert.Nil(t, err)

	httpmock.Reset()
	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"https://api-flower-power-pot.parrot.com/garden/v2/configuration",
			httpmock.NewBytesResponder(http.StatusOK, inputFile),
		),
	)

	_, err = getLocationsID(client, "access-token")
	assert.NotNil(t, err)
}

func TestURLS(t *testing.T) {

	f, err := NewFetcher()
	assert.Nil(t, err)

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	authFile, err := ioutil.ReadFile("data/valid_access.json")
	assert.Nil(t, err)

	locationFile, err := ioutil.ReadFile("data/valid_location.json")
	assert.Nil(t, err)

	expected := []string{
		"https://api-flower-power-pot.parrot.com/sensor_data/v6/sample/location/ADbiLOhuIA1426685634970?lat=56.4549073&lng=-2.9933931",
		"https://api-flower-power-pot.parrot.com/sensor_data/v6/sample/location/0SjBeS5ioY1426704625302?lat=56.4549073&lng=-2.9933931",
	}

	delay := time.Duration(0)

	httpmock.Reset()
	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"https://api-flower-power-pot.parrot.com/user/v1/authenticate?client_id=client-id&client_secret=client-secret&grant_type=password&password=password&username=username",
			httpmock.NewBytesResponder(http.StatusOK, authFile),
		),
	)

	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"https://api-flower-power-pot.parrot.com/garden/v2/configuration",
			httpmock.NewBytesResponder(http.StatusOK, locationFile),
		),
	)

	ctx := context.Background()
	ctx = context.WithValue(ctx, thingfulx.ClientToken, "token")

	got, err := f.URLS(ctx, client, delay)
	if assert.NoError(t, err) {
		assert.Equal(t, expected, got)
	}
}

func TestFetchValid(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	inputFile, err := ioutil.ReadFile("data/valid_sensor.json")
	assert.Nil(t, err)

	inputFile2, err := ioutil.ReadFile("data/empty_sensor.json")
	assert.Nil(t, err)

	authFile, err := ioutil.ReadFile("data/valid_access.json")
	assert.Nil(t, err)

	recordedAt, _ := time.Parse("2006-01-02T15:04:05Z", "2016-11-14T08:24:36Z")

	fetchURL, stubURL := makeTestFetchAndStubURLS(timeProvider)

	testcases := []struct {
		fetchURL       string
		stubURL        string
		respStatusCode int
		respBody       []byte // byte slice here as typically this data will be loaded from a file using ioutil.ReadFile
		expected       []thingfulx.Thing
	}{
		{
			fetchURL:       fetchURL,
			stubURL:        stubURL,
			respStatusCode: http.StatusOK,
			respBody:       inputFile,
			expected: []thingfulx.Thing{
				{
					Title:       "Parrot Flower Power soil sensor",
					Description: "Soil sensor data developed in conjunction with the GROW observatory. For more info see http://growobservatory.org/",
					Category:    &thingfulx.Environment,
					Webpage:     "http://global.parrot.com/au/products/flower-power/",
					DataURL:     "https://api-flower-power-pot.parrot.com/sensor_data/v6/sample/location/ADbiLOhuIA1426685634970?lat=56.4549073&lng=-2.9933931",
					IndexedAt:   timeProvider.Now(),
					Lng:         -2.9933931,
					Lat:         56.4549073,
					Provider:    fetcher.Provider(),
					Visibility:  thingfulx.Closed,
					Metadata:    "GROW,grower,science,permaculture,soil,sensor,moisture,temperature,humidity,plants,vegetables,smart home,garden,thingful:network=flowerpower",
					RawData:     inputFile,
					Channels: []thingfulx.Channel{
						thingfulx.Channel{
							ID:         "air_temperature",
							Value:      "19.62",
							RecordedAt: recordedAt,
							Unit:       "°C",
						},
						thingfulx.Channel{
							ID:         "fertilizer_level",
							Value:      "6.95",
							RecordedAt: recordedAt,
						},
						thingfulx.Channel{
							ID:         "light",
							Value:      "0.1",
							RecordedAt: recordedAt,
						},
						thingfulx.Channel{
							ID:         "soil_moisture",
							Value:      "23.49",
							RecordedAt: recordedAt,
							Unit:       "%",
						},
						thingfulx.Channel{
							ID:         "water_tank_level",
							Value:      "0",
							RecordedAt: recordedAt,
							Unit:       "%",
						},
					},
				},
			},
		},
		{
			fetchURL:       fetchURL,
			stubURL:        stubURL,
			respStatusCode: http.StatusOK,
			respBody:       inputFile2,
			expected: []thingfulx.Thing{
				{
					Title:       "Parrot Flower Power soil sensor",
					Description: "Soil sensor data developed in conjunction with the GROW observatory. For more info see http://growobservatory.org/",
					Category:    &thingfulx.Environment,
					Webpage:     "http://global.parrot.com/au/products/flower-power/",
					DataURL:     "https://api-flower-power-pot.parrot.com/sensor_data/v6/sample/location/ADbiLOhuIA1426685634970?lat=56.4549073&lng=-2.9933931",
					IndexedAt:   timeProvider.Now(),
					Lng:         -2.9933931,
					Lat:         56.4549073,
					Provider:    fetcher.Provider(),
					Visibility:  thingfulx.Closed,
					Metadata:    "GROW,grower,science,permaculture,soil,sensor,moisture,temperature,humidity,plants,vegetables,smart home,garden,thingful:network=flowerpower",
					RawData:     inputFile2,
				},
			},
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				"https://api-flower-power-pot.parrot.com/user/v1/authenticate?client_id=client-id&client_secret=client-secret&grant_type=password&password=password&username=username",
				httpmock.NewBytesResponder(http.StatusOK, authFile),
			),
		)

		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.stubURL,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		ctx := context.Background()
		ctx = context.WithValue(ctx, thingfulx.ClientToken, "token")

		things, err := fetcher.Fetch(ctx, testcase.fetchURL, client, timeProvider)
		if assert.NoError(t, err) {
			assert.Equal(t, testcase.expected, things)
		}
	}
}

func TestFetchInvalid(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	authFile, err := ioutil.ReadFile("data/valid_access.json")
	assert.Nil(t, err)

	fetchURL, stubURL := makeTestFetchAndStubURLS(timeProvider)

	testcases := []struct {
		stubURL        string
		fetchURL       string
		respStatusCode int
		respBody       string
	}{
		{
			stubURL:        stubURL,
			fetchURL:       fetchURL,
			respStatusCode: http.StatusNotFound,
			respBody:       "valid-data",
		},
		{
			stubURL:        stubURL,
			fetchURL:       fetchURL,
			respStatusCode: http.StatusOK,
			respBody:       "invalid-data",
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				"https://api-flower-power-pot.parrot.com/user/v1/authenticate?client_id=client-id&client_secret=client-secret&grant_type=password&password=password&username=username",
				httpmock.NewBytesResponder(http.StatusOK, authFile),
			),
		)

		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.fetchURL,
				httpmock.NewStringResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		ctx := context.Background()
		ctx = context.WithValue(ctx, thingfulx.ClientToken, "token")

		_, err := fetcher.Fetch(ctx, testcase.stubURL, client, timeProvider)
		assert.Error(t, err)
	}
}

func TestFetchResponseError(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	fetchURL, stubURL := makeTestFetchAndStubURLS(timeProvider)

	authFile, err := ioutil.ReadFile("data/valid_access.json")
	assert.Nil(t, err)

	testcases := []struct {
		stubURL        string
		fetchURL       string
		respStatusCode int
		expected       *thingfulx.ErrUnexpectedResponse
	}{
		{
			stubURL:        stubURL,
			fetchURL:       fetchURL,
			respStatusCode: http.StatusNotFound,
			expected:       thingfulx.ErrNotFound,
		},
		{
			stubURL:        stubURL,
			fetchURL:       fetchURL,
			respStatusCode: http.StatusRequestTimeout,
			expected:       thingfulx.NewErrUnexpectedResponse("408"),
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				"https://api-flower-power-pot.parrot.com/user/v1/authenticate?client_id=client-id&client_secret=client-secret&grant_type=password&password=password&username=username",
				httpmock.NewBytesResponder(http.StatusOK, authFile),
			),
		)

		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.stubURL,
				httpmock.NewBytesResponder(testcase.respStatusCode, []byte{}),
			),
		)

		ctx := context.Background()
		ctx = context.WithValue(ctx, thingfulx.ClientToken, "token")

		_, err := fetcher.Fetch(ctx, testcase.fetchURL, client, timeProvider)
		if assert.Error(t, err) {
			assert.Equal(t, testcase.expected, err)
		}
	}
}

func TestFetchError(t *testing.T) {

	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testcases := []string{"", "%"}

	for _, url := range testcases {
		_, err := fetcher.Fetch(context.Background(), url, client, timeProvider)
		assert.Error(t, err)
	}
}
