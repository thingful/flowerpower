// Package flowerpower is a fetcher that collect soil sensor data
// from Parrot flower power sensors
package flowerpower

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/thingful/thingfulx"
	"golang.org/x/net/context"
)

const (
	// AuthURL is the API endpoint we query to obtain access tokens
	AuthURL = "https://api-flower-power-pot.parrot.com/user/v1/authenticate"

	// LocationsURL is the API endpoint we query to obtain geolocation data
	LocationsURL = "https://api-flower-power-pot.parrot.com/garden/v2/configuration"

	// SensorDataURL is the API endpoint we query to obtain sensor data
	SensorDataURL = "https://api-flower-power-pot.parrot.com/sensor_data/v6/sample/location/"

	// NumberOfDaysToQuery is the number of days from now that the fetcher will
	// retrieve data from
	NumberOfDaysToQuery = 3

	// Metadata is some arbitrary metadata we add to all things indexed by this
	// fetcher. Specifically it should include the machine tag type string that
	// looks like this:
	Metadata = "GROW,grower,science,permaculture,soil,sensor,moisture,temperature,humidity,plants,vegetables,smart home,garden,thingful:network=flowerpower"
)

type authData struct {
	Token        string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Error        string `json:"error"`
	ErrorMsg     string `json:"error_description"`
}

type locationData struct {
	Locations []location `json:"locations"`
}

type location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Id        string  `json:"location_identifier"`
	Sensor    sensor  `json:"sensor"`
}

type sensor struct {
	Type string `json:"sensor_identifier"`
}

type sensorData struct {
	Samples []sample `json:"samples"`
}

type sample struct {
	AirTemp         float64 `json:"air_temperature_celsius"`
	RecordedAt      string  `json:"capture_datetime_utc"`
	FertilizerLevel float64 `json:"fertilizer_level"`
	Light           float64 `json:"light"`
	SoilMoisture    float64 `json:"soil_moisture_percent"`
	WaterTankLevel  float64 `json:"water_tank_level_percent"`
}

// NewFetcher instantiates a new Fetcher instance. All fetchers must try to
// load any required config values from namespaced environment variables.
func NewFetcher() (thingfulx.Fetcher, error) {
	// we know these URLs won't give an error
	providerURL, _ := url.Parse("https://www.parrot.com/")

	provider := &thingfulx.Provider{
		Name:        "Parrot - Flower Power",
		UID:         "flowerpower",
		Webpage:     providerURL,
		Description: "Parrot SA is a French wireless products manufacturer company specialized in technologies involving voice recognition, signal processing for embedded products and drones.",
	}

	return &fetcher{
		provider: provider,
	}, nil
}

type fetcher struct {
	provider     *thingfulx.Provider
	accessToken  string
	refreshToken string
	user         string // not needed unless user must authenticate
	password     string // not needed unless user must authenticate
	clientID     string
	clientSecret string
}

// Provider is a function that returns an instantiated Provider object
// describing the upstream data provider
func (f fetcher) Provider() *thingfulx.Provider {
	return f.provider
}

type requestObj struct {
	URL        string
	Client     thingfulx.Client
	AuthHeader string
}

// URLS returns the minimum set of URLs required to fully index this data provider
func (f fetcher) URLS(ctx context.Context, client thingfulx.Client, delay time.Duration) ([]string, error) {
	var URLs []string

	token, err := validateToken(ctx.Value(thingfulx.ClientToken))
	if err != nil {
		return nil, err
	}

	f.accessToken = token

	locations, err := getLocationsID(client, f.accessToken)
	if err != nil {
		return nil, err
	}

	for _, loc := range locations {

		dataURL := SensorDataURL + loc.Id
		u, _ := url.Parse(dataURL)
		v := url.Values{}
		v.Set("lat", strconv.FormatFloat(loc.Latitude, 'f', -1, 64))
		v.Add("lng", strconv.FormatFloat(loc.Longitude, 'f', -1, 64))

		u.RawQuery = v.Encode()

		URLs = append(URLs, u.String())
	}

	return URLs, nil
}

// Fetch a resource from the upstream provider if we can.
func (f fetcher) Fetch(ctx context.Context, url string, client thingfulx.Client, timeProvider thingfulx.TimeProvider) ([]thingfulx.Thing, error) {

	// ensure token is present and is valid
	token, err := validateToken(ctx.Value(thingfulx.ClientToken))
	if err != nil {
		return nil, err
	}

	f.accessToken = token

	// remove query parameters and 'from' and 'to' time parameters to the
	// resulting base url
	urlNoQuery := strings.Split(url, "?")[0]
	u, err := buildSensorDataURL(urlNoQuery, timeProvider)
	if err != nil {
		return nil, err
	}

	reqObj := requestObj{
		URL:        u,
		Client:     client,
		AuthHeader: f.accessToken,
	}

	b, err := makeHTTPRequest(reqObj)
	if err != nil {
		return nil, err
	}

	return f.parse(url, b, timeProvider)
}

// parse is our function to actually extract data and return the slice of things. we should
func (f *fetcher) parse(rawURL string, data []byte, timeProvider thingfulx.TimeProvider) ([]thingfulx.Thing, error) {
	things := []thingfulx.Thing{}
	var sData sensorData
	var latestSample sample

	err := json.Unmarshal(data, &sData)
	if err != nil {
		return nil, err
	}

	// parse url and extract data from query
	u, err := url.Parse(rawURL)
	if err != nil {
		return nil, err
	}

	q := u.Query()
	lat, err := strconv.ParseFloat(q.Get("lat"), 64)
	if err != nil {
		return nil, err
	}

	lng, err := strconv.ParseFloat(q.Get("lng"), 64)
	if err != nil {
		return nil, err
	}

	thing := thingfulx.Thing{
		Title:       "Parrot Flower Power soil sensor",
		Description: "Soil sensor data developed in conjunction with the GROW observatory. For more info see http://growobservatory.org/",
		Category:    &thingfulx.Environment,
		Webpage:     "http://global.parrot.com/au/products/flower-power/",
		DataURL:     rawURL,
		IndexedAt:   timeProvider.Now(),
		Lng:         lng,
		Lat:         lat,
		Provider:    f.provider,
		Visibility:  thingfulx.Closed,
		Metadata:    Metadata,
		RawData:     data,
	}

	// if sensor data slice is empty return thing without channels
	if len(sData.Samples) == 0 {
		things = append(things, thing)
		return things, nil
	}
	latestSample = sData.Samples[len(sData.Samples)-1]

	channels, err := buildChannels(latestSample)
	if err == nil {
		thing.Channels = channels
	}

	things = append(things, thing)

	return things, nil
}

func makeHTTPRequest(r requestObj) ([]byte, error) {
	req, err := http.NewRequest("GET", r.URL, nil)
	if err != nil {
		return nil, err
	}

	// set header if authorization token has been set
	if r.AuthHeader != "" {
		req.Header.Set("Authorization", "Bearer "+r.AuthHeader)
	}

	res, err := r.Client.DoHTTP(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		if cerr := res.Body.Close(); err == nil && cerr != nil {
			err = cerr
		}
	}()

	if res.StatusCode != http.StatusOK {
		if res.StatusCode == http.StatusNotFound {
			return nil, thingfulx.ErrNotFound
		}
		return nil, thingfulx.NewErrUnexpectedResponse(res.Status)
	}

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func authenticate(client thingfulx.Client, f *fetcher) error {
	var auth authData

	authURL, _ := url.Parse(AuthURL)
	v := url.Values{}
	v.Set("grant_type", "password")
	v.Add("username", f.user)
	v.Add("password", f.password)
	v.Add("client_id", f.clientID)
	v.Add("client_secret", f.clientSecret)

	authURL.RawQuery = v.Encode()

	r := requestObj{
		URL:    authURL.String(),
		Client: client,
	}

	b, err := makeHTTPRequest(r)
	if err != nil {
		return err
	}

	err = json.Unmarshal(b, &auth)
	if err != nil {
		return err
	}

	if auth.Error != "" {
		return errors.New(auth.ErrorMsg)
	}

	f.accessToken = auth.Token
	f.refreshToken = auth.RefreshToken

	return nil
}

func getLocationsID(client thingfulx.Client, authToken string) ([]location, error) {
	var l locationData
	var activeSensors []location

	r := requestObj{
		URL:        LocationsURL,
		Client:     client,
		AuthHeader: authToken,
	}

	b, err := makeHTTPRequest(r)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(b, &l)
	if err != nil {
		return nil, err
	}

	for _, l := range l.Locations {
		if l.Sensor.Type != "" {
			activeSensors = append(activeSensors, l)
		}
	}

	return activeSensors, nil
}

func buildSensorDataURL(baseURL string, timeProvider thingfulx.TimeProvider) (string, error) {
	u, err := url.Parse(baseURL)
	if err != nil {
		return "", err
	}

	data_from := timeProvider.Now().Add(time.Hour * 24 * -NumberOfDaysToQuery).Format("2006-01-02T15:04:05Z")
	data_to := timeProvider.Now().Format("2006-01-02T15:04:05Z")

	v := url.Values{}
	v.Set("from_datetime_utc", data_from)
	v.Add("to_datetime_utc", data_to)

	u.RawQuery = v.Encode()

	return u.String(), nil
}

func buildChannels(rawData sample) ([]thingfulx.Channel, error) {
	timeLayout := "2006-01-02T15:04:05Z"

	recordedAt, err := time.Parse(timeLayout, rawData.RecordedAt)
	if err != nil {
		return nil, err
	}

	channels := []thingfulx.Channel{
		thingfulx.Channel{
			ID:         "air_temperature",
			Value:      strconv.FormatFloat(rawData.AirTemp, 'f', -1, 64),
			RecordedAt: recordedAt,
			Unit:       "°C",
		},
		thingfulx.Channel{
			ID:         "fertilizer_level",
			Value:      strconv.FormatFloat(rawData.FertilizerLevel, 'f', -1, 64),
			RecordedAt: recordedAt,
		},
		thingfulx.Channel{
			ID:         "light",
			Value:      strconv.FormatFloat(rawData.Light, 'f', -1, 64),
			RecordedAt: recordedAt,
		},
		thingfulx.Channel{
			ID:         "soil_moisture",
			Value:      strconv.FormatFloat(rawData.SoilMoisture, 'f', -1, 64),
			RecordedAt: recordedAt,
			Unit:       "%",
		},
		thingfulx.Channel{
			ID:         "water_tank_level",
			Value:      strconv.FormatFloat(rawData.WaterTankLevel, 'f', -1, 64),
			RecordedAt: recordedAt,
			Unit:       "%",
		},
	}

	return channels, nil
}

func validateToken(t interface{}) (string, error) {
	token, ok := t.(string)

	if !ok {
		return "", thingfulx.NewErrMissingConfig(fmt.Sprintf("'%s' must be provided and must be of type 'string'", thingfulx.ClientToken))
	}

	if token == "" {
		return "", thingfulx.NewErrMissingConfig(fmt.Sprintf("'%s' cannot be an empty string", thingfulx.ClientToken))
	}

	return token, nil
}
