# Parrot - Flower Power

A Thingful fetcher for getting data from the api-flower-power-pot.parrot.com API.
This API returns data about soil sensors.


## GROW Observatory Project
This fetcher was develop as part of the GROW Observatory project. More info can be found at the [GROW Website](http://growobservatory.org/)